const express = require('express');
const router = express.Router();

router.get('/acme-challenge/NkFhr4iae8ZKvLH1HmZUJd6N6emdCPqkWcsY1OKq5wg', function (req, res, next) {
	res.status(200).send('NkFhr4iae8ZKvLH1HmZUJd6N6emdCPqkWcsY1OKq5wg.3tgww2nNYhlpDtiAiLx9yZ1x_Hkz5i-HKgOB5ixWEgo');
	next();
});

router.get('/acme-challenge/96kybAH9Qbj-z2MPj_6HjHWsyGH8Xvpc8lZLdlBYTUo', function (req, res, next) {
	res.status(200).send('96kybAH9Qbj-z2MPj_6HjHWsyGH8Xvpc8lZLdlBYTUo.3tgww2nNYhlpDtiAiLx9yZ1x_Hkz5i-HKgOB5ixWEgo');
	next();
});

router.get('/acme-challenge/:id', function (req, res) {
	res.status(200).send(req.params.id);
});

module.exports = router;