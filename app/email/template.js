var exports = module.exports = {};
const pug = require('pug');

exports.renderHtmlEmail = function (template, data) {
    return pug.renderFile(global.appRoot + "/app/email/templates/" + template + ".pug", data);
};