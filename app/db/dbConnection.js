var exports = module.exports = {};

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var db = mongoose.connection;

db.on('error', console.error);
db.once('open', function () {
    console.log("Database \"" + process.env.MONGODB_URI + "\" connected");
});
mongoose.connect(process.env.MONGODB_URI);

exports.getConnection = function() { return mongoose; }