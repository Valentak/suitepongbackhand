var STATUS_AVAILABLE = "available";
var STATUS_BOOKED = "booked";
var STATUS_CLOSE_TO_BOOKING = "close-to-booking";
var CLOSE_TO_BOOKING_THRESHOLD = 1000 * 10 * 60; // in milliseconds
var LONG_TIME_THRESHOLD = 1000 * 30 * 60; // in milliseconds

var NEW_BOOKING_BUTTONS = [
    {id: '#book-10', duration: 10},
    {id: '#book-15', duration: 15},
    {id: '#book-20', duration: 20}
];

$(document).ready(function() {
    pongUtil.callAndSetInterval(updateCurrentTime, 5);
    pongUtil.callAndSetInterval(updateBookingStatus, 20);

    $.each(NEW_BOOKING_BUTTONS, function(index, button) {
        $(button.id).click(function() {
            if (!$(this).hasClass("disabledx"))
                submitNewBooking(button.duration);
        });
    });

    $("#confirm-booking").find("button").click(function() {
        confirmBooking();
    });
});

/**
 * Returns the current booking status as a string.
 * @param currentTime (Date) Current server time.
 * @param currentOrNearestBookingStart (Date) The starting date/time of the current or the nearest future booking.
 * @return "available", "booked", or "close-to-booking" (if the nearest booking is in less than 10 minutes)
 */
function calculateBookingStatus(currentTime, currentOrNearestBookingStart) {
    var startOffset = currentOrNearestBookingStart.getTime() - currentTime.getTime();

    if (startOffset > CLOSE_TO_BOOKING_THRESHOLD)
        return STATUS_AVAILABLE;
    else if (startOffset <= 0)
        return STATUS_BOOKED;
    else
        return STATUS_CLOSE_TO_BOOKING;
}

/**
 * Returns the current booking status as text, such as "Available for the next 18 minutes" or "Booked until 10:55".
 * @param currentTime (Date) Current server time.
 * @param currentOrNearestBookingStart (Date) The starting date/time of the current or the nearest future booking.
 * @param currentOrNearestBookingSequenceEnd (Date) The end date/time of the current or the nearest future sequence
 *   of consecutive bookings.
 */
function calculateBookingStatusDetail(currentTime, currentOrNearestBookingStart, currentOrNearestBookingSequenceEnd) {
    var currentTimestamp = currentTime.getTime();
    var startOffset = currentOrNearestBookingStart.getTime() - currentTimestamp;
    var endOffset = currentOrNearestBookingSequenceEnd.getTime() - currentTimestamp;

    if (startOffset > CLOSE_TO_BOOKING_THRESHOLD) {
        if (startOffset < LONG_TIME_THRESHOLD)
            return "Available for the next " + Math.round(startOffset / 60 / 1000) + " minutes";
        else
            return "Available until " + formatTimeToHoursAndMinutes(currentOrNearestBookingStart);
    }
    else {
        if (endOffset < LONG_TIME_THRESHOLD)
            return "Available in " + Math.round(endOffset / 60 / 1000) + " minutes";
        else
            return "Booked until " + formatTimeToHoursAndMinutes(currentOrNearestBookingSequenceEnd);
    }
}

function updateBookingStatus() {
    $.get("/v1/time", function(currentTimeAsString) {
    $.get("/v1/booking/query/current", function(booking) {
        var currentTime = new Date(currentTimeAsString);
        setCurrentBooking(currentTime, booking);
        setNewBookingButtonsVisibility(currentTime, booking);
        enableOrDisableNewBookingButtons(currentTime, booking);
        setConfirmButtonVisibility(currentTime, booking);

        if (!booking) {
            setBookingStatus(STATUS_AVAILABLE, "Available");
        }
        else {
            var bookingStatus = calculateBookingStatus(currentTime, new Date(booking.start));
            if (bookingStatus == STATUS_AVAILABLE) {
                var bookingStatusDetail = calculateBookingStatusDetail(
                    currentTime, new Date(booking.start), new Date(booking.end));
                setBookingStatus(bookingStatus, bookingStatusDetail);
            }
            else {
                $.get("/v1/booking/query/nextfree", function(nextFree) {
                    var bookingStatusDetail = calculateBookingStatusDetail(
                        currentTime, new Date(booking.start), new Date(nextFree));
                    setBookingStatus(bookingStatus, bookingStatusDetail);
                });
            }
        }
    })});
}

function submitNewBooking(durationInMinutes) {
    $.post("/v1/booking/now", {duration: durationInMinutes})
        .done(function(data) {
            console.log("new booking success:\n" + JSON.stringify(data));
            updateBookingStatus();
        })
        .fail(function(data) {
            console.log("new booking error:\n" + JSON.stringify(data));
            showError(data.responseText);
        });
}

function confirmBooking() {
    $.get("/v1/booking/query/current", function(booking) {
        if (booking && !booking.confirmed) {
            $.post("/v1/booking/" + booking.id + "/confirm")
                    .done(function(data) {
                        console.log("booking confirmation success:\n" + JSON.stringify(data));
                        updateBookingStatus();
                    })
                    .fail(function(data) {
                        console.log("booking confirmation error:\n" + JSON.stringify(data));
                        showError(data.responseText);
                    });
        }
    });
}

function setNewBookingButtonsVisibility(currentTime, booking) {
    var bookingStatus = booking
        ? calculateBookingStatus(currentTime, new Date(booking.start))
        : STATUS_AVAILABLE;
    $("#new-booking").toggle(bookingStatus == STATUS_AVAILABLE);
}

function enableOrDisableNewBookingButtons(currentTime, booking) {
    var millisecondsToNearestBooking = booking
        ? new Date(booking.start).getTime() - currentTime.getTime()
        : Number.MAX_VALUE;

    $.each(NEW_BOOKING_BUTTONS, function(index, button) {
        var enabled = (button.duration * 60 * 1000 < millisecondsToNearestBooking);
        $(button.id).toggleClass("disabled", !enabled);
    });
}

function setConfirmButtonVisibility(currentTime, booking) {
    var bookingStatus = booking
            ? calculateBookingStatus(currentTime, new Date(booking.start))
            : STATUS_AVAILABLE;
    var shouldBeVisible = bookingStatus == STATUS_BOOKED && !booking.confirmed;
    $("#confirm-booking").toggle(shouldBeVisible);
}

function setBookingStatus(bookingStatus, bookingStatusDetail) {
    $("#main-container")
        .removeClass("booked-bg available-bg close-to-booking-bg")
        .addClass(bookingStatus + "-bg");

    $("#booking-status")
        .removeClass("booked-fg available-fg close-to-booking-fg")
        .addClass(bookingStatus + "-fg")
        .html(bookingStatusDetail);
}

function setCurrentBooking(currentTime, booking) {
    if (!booking) {
        $("#booking-details")
            .empty()
            .append("<p id='booking-heading' class='available-fg'>Next Match</p>")
            .append("<p>not any time soon</p>");
    }
    else {
        var startTime = new Date(booking.start);
        var bookingStatus = calculateBookingStatus(currentTime, startTime);
        var startTimeAsString = formatTimeToHoursAndMinutes(startTime);
        var endTimeAsString = formatTimeToHoursAndMinutes(new Date(booking.end));
        var bookingTime = startTimeAsString + " - " + endTimeAsString;
        var bookingHeadingText = bookingStatus == STATUS_BOOKED ? "Current Match" : "Next Match";

        $("#booking-details")
            .empty()
            .append("<p id='booking-heading' class='" + bookingStatus + "-fg'>" + bookingHeadingText + "</p>")
            .append("<p id='booking-time'>" + bookingTime + "</p>")
            .append("<p id='booking-name'>" + booking.userName + "</p>");
    }
}

function updateCurrentTime() {
    $.get("/v1/time", function(currentTimeAsString) {
        $("#current-time").html(formatTimeToHoursAndMinutes(new Date(currentTimeAsString)));
    });
}

function formatTimeToHoursAndMinutes(date) {
    return date.getHours() + ":" + zeroPad(date.getMinutes());
}

function zeroPad(number) {
    if (number < 10)
        return "0" + number;
    else
        return "" + number;
}

function showError(message) {
    $.msgBox({
        title: "Error",
        content: message,
        type: "error",
        opacity: 0.5,
        autoClose: true
    });
}