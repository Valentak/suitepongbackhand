function HeaderUpdater() {
	const CLOSE_TO_BOOKING_THRESHOLD = 10; // minutes

	this.update = function() {
		$.get("v1/booking/query/current", processBookingAndUpdateHeader);
	};

	function processBookingAndUpdateHeader(booking) {
		var $headerUntil = $("#currentStatus").find("#until");
		if (!booking) {
			updateHtml({
				showTime: false
			});
			return;
		}

		$headerUntil.removeClass("hidden");
		var now = new Date();
		var start = new Date(booking.start);
		if (now < dateUtil.addMinutes(start, -1 * CLOSE_TO_BOOKING_THRESHOLD)) {
			updateHtml({
				time: dateUtil.formatTime(start),
				minutes: dateUtil.getMinutesBetween(now, dateUtil.roundDownToFiveMinutes(start)),
				occupied: false,
				showTime: true
			});
		} else {
			$.get({
				url: "v1/booking/query/nextfree",
				success: function(data) {
					var nextFree = new Date(data);
					updateHtml({
						time: dateUtil.formatTime(nextFree),
						minutes: dateUtil.getMinutesBetween(now, dateUtil.roundUpToFiveMinutes(nextFree)),
						occupied: true,
						showTime: true
					});
				}
			});
		}
	}

	function updateHtml(status) {
		var $statusElement = $("#currentStatus");
		if (status.occupied) {
			$statusElement.removeClass("background-free");
			$statusElement.addClass("background-occupied");
			$statusElement.find("#status strong").text("OCCUPIED");
		} else {
			$statusElement.removeClass("background-occupied");
			$statusElement.addClass("background-free");
			$statusElement.find("#status strong").text("FREE");
		}
		$statusElement.find("#until strong").text(status.time);
		$statusElement.find("#until span").text(status.minutes);
		$statusElement.find("#until").toggle(status.showTime);
	}
}