function PongUtil() {
    this.showMessage = function(message, type, afterCloseCode) {
        $.msgBox({
            content: message,
            type: type,
            opacity: 0.5,
            afterClose: afterCloseCode
        });
    };

    this.confirmDialogue = function(message, onConfirmCode, onCancelCode) {
        $.msgBox({
            content: message,
            type: "confirm",
            buttons: [{value: "Yes"}, {value: "No"}],
            success: function (result) {
                if (result == "Yes" && onConfirmCode)
                    onConfirmCode();
                if (result == "No" && onCancelCode)
                    onCancelCode();
            },
            opacity: 0.5
        });
    };

    this.callAndSetInterval = function(code, delayInSeconds) {
        code();
        window.setInterval(code, delayInSeconds * 1000);
    };

    this.getParameterByName = function(name) {
        var match = new RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    };
}

pongUtil = new PongUtil();