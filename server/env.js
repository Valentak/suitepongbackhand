const logger = require('winston');
const path = require('path');

const defaultConfig = {
	test: {
		DEBUG_LEVEL: 'debug',
		MONGODB_URI: 'mongodb://0.0.0.0:27017/suitepong',
		SENDGRID_API_KEY: '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',
		EMAIL_SERVICE_PROVIDER: 'dummy'
	},
	development: {
		DEBUG_LEVEL: 'info',
		SENDGRID_API_KEY: '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',
		EMAIL_SERVICE_PROVIDER: 'dummy'
	},
	production: {
		DEBUG_LEVEL: 'error',
		SENDGRID_API_KEY: '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',
		EMAIL_SERVICE_PROVIDER: 'sendGrid'
	}
};

function log() {
	logger.level = process.env.DEBUG_LEVEL;
	logger.log('info', 'DEBUG_LEVEL: ' + process.env.DEBUG_LEVEL);
	logger.log('info', 'MONGODB_URI: ' + process.env.MONGODB_URI);
	logger.log('info', 'SENDGRID_API_KEY: ' + process.env.SENDGRID_API_KEY);
	logger.log('info', 'EMAIL_SERVICE_PROVIDER: ' + process.env.EMAIL_SERVICE_PROVIDER);
	logger.log('debug', 'global.appRoot: ' + global.appRoot);
}

function verify() {
	if (!process.env.DEBUG_LEVEL || !process.env.MONGODB_URI || !process.env.SENDGRID_API_KEY) {
		log();
		throw new Error('Invalid configuration');
	}
}

function setUp() {
	global.appRoot = path.resolve(__dirname) + '/..';

	const selectedConfig = defaultConfig[process.env.NODE_ENV];
	if (!selectedConfig)
		throw new Error("No default config found");

	if (!process.env.DEBUG_LEVEL)
		process.env.DEBUG_LEVEL = selectedConfig.DEBUG_LEVEL;

	if (!process.env.MONGODB_URI)
		process.env.MONGODB_URI = selectedConfig.MONGODB_URI;

	if (!process.env.SENDGRID_API_KEY)
		process.env.SENDGRID_API_KEY = selectedConfig.SENDGRID_API_KEY;

    if (!process.env.EMAIL_SERVICE_PROVIDER)
        process.env.EMAIL_SERVICE_PROVIDER= selectedConfig.EMAIL_SERVICE_PROVIDER;
}

setUp();
verify();
log();
