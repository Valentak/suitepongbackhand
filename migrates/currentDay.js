var chai = require('chai');
var chaihttp = require('chai-http');
chai.use(chaihttp);
var app = require('../server/server.js');

const minute = 60 * 1000;
const hour = 60 * minute;
const day = 24 * hour;
const startTime = new Date((parseInt(new Date().getTime() / day) * day) + 8 * hour);


var bookings = [
	{
		start: startTime,
		end: new Date(startTime.getTime() + hour),
		userName: 'Michal'
	},
	{
		start: new Date(startTime.getTime() + 2 * hour + 15 * minute),
		end: new Date(startTime.getTime() + 2 * hour + 30 * minute),
		userName: 'Michal'
	},
	{
		start: new Date(startTime.getTime() + 3 * hour + 10 * minute),
		end: new Date(startTime.getTime() + 3 * hour + 30 * minute),
		userName: 'Michal'
	},
	{
		start: new Date(startTime.getTime() + 5 * hour + 20 * minute),
		end: new Date(startTime.getTime() + 5 * hour + 30 * minute),
		userName: 'Michal'
	},
	{
		start: new Date(startTime.getTime() + 8 * hour + 10 * minute),
		end: new Date(startTime.getTime() + 8 * hour + 45 * minute),
		userName: 'Michal'
	}
];

var close = bookings.length;
bookings.map(function(booking) {
	chai.request(app)
		.post('/v1/booking')
		.send(booking)
		.end(function(err, res) {
			if(--close) process.exit();
		});
});


