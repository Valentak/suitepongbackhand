process.env.NODE_ENV = 'test';

var sinon = require('sinon');

var emailApi = require("../../app/email/service/" + process.env.EMAIL_SERVICE_PROVIDER + ".js");
var emailTemplateClass = require("../../app/email/template.js");
var email = require("../../app/email/email.js");
var userDaoClass = require("../../app/db/userDao.js");

describe('sendLoginEmail', function () {
	it('should create login email and pass it to emailAPIs send function', sinon.test( function () {
        var emailSend = this.stub(emailApi, 'send');
        var emailTemplate = this.stub(emailTemplateClass, 'renderHtmlEmail');
        emailTemplate.returns("email body");
        var userDao = this.stub(userDaoClass, 'getByUserName');
        userDao.yields(null, {'candidateSessionId': 'candidateSessionId'});
        var callback = this.spy();

        email.sendLoginEmail('test', callback);

        sinon.assert.calledOnce(emailSend);
        var emailDto = {
            'email': "test <test@netsuite.com>",
            'subject': "SuitePong login confirmation",
            'content': "email body"
        }
        sinon.assert.calledWithExactly(emailSend, emailDto, callback);
	}));
});

describe('sendBookingConfirmationEmail', function () {
	it('should create confirmation email and pass it to emailAPIs send function', sinon.test( function() {
        var emailSend = this.stub(emailApi, 'send');
        var emailTemplate = this.stub(emailTemplateClass, 'renderHtmlEmail');
        emailTemplate.returns("email body");
        var callback = this.spy();
        var booking = {
            'userName': 'test'
        };

        email.sendBookingConfirmationEmail(booking, callback);

        sinon.assert.calledOnce(emailSend);
        var emailDto = {
            'email': "test <test@netsuite.com>",
            'subject': "SuitePong booking confirmation",
            'content': "email body"
        }
        sinon.assert.calledWithExactly(emailSend, emailDto, callback);
	}));
});