process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaihttp = require('chai-http');
var should = chai.should();

chai.use(chaihttp);

var app = require('../../server/server.js');

describe('get /v1/time', function () {
	it('should return 200 response', function (done) {
		chai.request(app)
			.get('/v1/time')
			.end(function (err, res) {
				res.should.have.status(200);
				done();
			});
	});
});